package com.isuisudmt;

public interface ConnectionLostCallback {
    public void connectionLost();
}
