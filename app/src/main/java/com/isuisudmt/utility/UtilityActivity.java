package com.isuisudmt.utility;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.dmt.MyErrorMessage;
import com.isuisudmt.recharge.ReacargeStatusActivity;
import com.isuisudmt.utils.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.isuisudmt.BuildConfig.GET_AEPS_FETCH_BILL_AMOUNT_URL;
import static com.isuisudmt.BuildConfig.GET_AEPS_RECHARGE_URL;

public class UtilityActivity extends AppCompatActivity implements View.OnClickListener {

    String mobilenoString, amountString, operatorString, rechargetypeString, stdCodeString, pincodeString, accountNoString, authString, latitudeString,
            circleCodeString, longitudeString;

    String[] operatorUtility = {"MTNL Delhi LandLine Utility", "BSNL Landline Utility", "Reliance Energy(Mumbai) Utility", "MSEDC Limited Utility",
            "Torrent Power Utility", "Mahanagar Gas Limited Utility", "Tata AIG Life Utility", "ICICI Pru Life Utility", "BSES Rajdhani", "BSES Yamuna",
            "North Delhi Power Limited", "Brihan Mumbai Electric Supply and Transport Undertaking", "Rajasthan Vidyut Vitran Nigam Limited",
            "Soouthern power Distribution Company Ltd of Andhra Pradesh( APSPDCL)", "Bangalore Electricity Supply Company", "Madhya Pradesh Madhya Kshetra Vidyut Vitaran Company Limited - Bhopal",
            "Noida Power Company Limited", "Madhya Pradesh Paschim Kshetra Vidyut Vitaran Indor", "Calcutta Electricity Supply Ltd", "Chhattisgarh State Electricity Board", "India Power Corporation Limited",
            "Jamshedpur Utilities and Services Company Limited", "IGL (Indraprast Gas Limited)", "Gujarat Gas company Limited", "ADANI GAS",
            "Tikona Postpaid", "Hathway Broadband Retail"};

    String[] serviceType = {"Landline Individual", "Landline Corporate"};

    TextInputEditText utility_mobileno, utility_mobilenoo, utility_mobilenooo, utility_select_operator, utility_select_Spinner;
    Button utility_button, utility_fetch_bill_button;
    TextView utility_amount;
    TextInputLayout layout_utility_mobileno, layout_utility_mobilenoo,layout_utility_mobilenooo,layout_utility_select_Spinner;

    private APIService apiService;
    SessionManager session;
    String _token = "", _admin = "";
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Utility");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        utility_mobileno = findViewById(R.id.utility_mobileno);
        utility_mobilenoo = findViewById(R.id.utility_mobilenoo);
        utility_mobilenooo = findViewById(R.id.utility_mobilenooo);

        utility_select_Spinner = findViewById(R.id.utility_select_Spinner);
        utility_select_operator = findViewById(R.id.utility_select_operator);

        utility_button = findViewById(R.id.utility_button);
        utility_fetch_bill_button = findViewById(R.id.utility_fetch_bill_button);
        layout_utility_select_Spinner = findViewById(R.id.layout_utility_select_Spinner);

        utility_amount = findViewById(R.id.utility_amount);

        layout_utility_mobileno = findViewById(R.id.layout_utility_mobileno);
        layout_utility_mobilenoo = findViewById(R.id.layout_utility_mobilenoo);
        layout_utility_mobilenooo = findViewById(R.id.layout_utility_mobilenooo);

        rechargetypeString = "Utility_bills";
        stdCodeString = "";
        pincodeString = "";
        accountNoString = "";
        latitudeString = "";
        circleCodeString = "";
        longitudeString = "";
        authString = "";

        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        utility_fetch_bill_button.setOnClickListener(this);
        utility_button.setOnClickListener(this);

        utility_mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    mobilenoString = null;
                } else {
                    mobilenoString = utility_mobileno.getText().toString();
                }
            }
        });

        utility_mobilenoo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    accountNoString = null;
                } else {
                    accountNoString = utility_mobilenoo.getText().toString();
                }
            }
        });

        utility_mobilenooo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    authString = null;
                } else {
                    authString = utility_mobilenooo.getText().toString();
                }
            }
        });

        utility_select_Spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View _view = getLayoutInflater().inflate(R.layout.custom_postpaid_operator, null);
                BottomSheetDialog dialog = new BottomSheetDialog(UtilityActivity.this);
                dialog.setContentView(_view);

                ListView listView = _view.findViewById(R.id.listing);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UtilityActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, serviceType);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String itemValue = (String) listView.getItemAtPosition(position);
                        String operatorString = itemValue;
                        utility_select_Spinner.setText(itemValue);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        utility_select_operator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View _view = getLayoutInflater().inflate(R.layout.custom_postpaid_operator, null);
                BottomSheetDialog dialog = new BottomSheetDialog(UtilityActivity.this);
                dialog.setContentView(_view);

                ListView listView = _view.findViewById(R.id.listing);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UtilityActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, operatorUtility);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                        utility_select_operator.setText(operatorUtility[i]);

                        layout_utility_mobilenoo.setVisibility(View.GONE);
                        layout_utility_mobilenooo.setVisibility(View.GONE);
                        layout_utility_select_Spinner.setVisibility(View.GONE);

                        operatorString = operatorUtility[i];
                        if (i == 0) {
                            layout_utility_mobilenoo.setVisibility(View.VISIBLE);
                            layout_utility_mobileno.setHint("Phone number");
                            layout_utility_mobilenoo.setHint("Customer Account Number");

                        } else if (i == 1) {

                            layout_utility_mobilenoo.setVisibility(View.VISIBLE);
                            layout_utility_select_Spinner.setVisibility(View.VISIBLE);

                            layout_utility_mobileno.setHint("Phone number");
                            layout_utility_mobilenoo.setHint("Account Number");
                        } else if (i == 2) {
                            layout_utility_mobilenoo.setVisibility(View.VISIBLE);

                            layout_utility_mobileno.setHint("Customer number");
                            layout_utility_mobilenoo.setHint("Cycle Number");

                        } else if (i == 3) {
                            layout_utility_mobilenoo.setVisibility(View.VISIBLE);
                            layout_utility_mobilenooo.setVisibility(View.VISIBLE);

                            layout_utility_mobileno.setHint("Customer number");
                            layout_utility_mobilenoo.setHint("Billing Unit");
                            layout_utility_mobilenooo.setHint("Processing Cycle");


                        } else if (i == 4) {
                            layout_utility_mobilenoo.setVisibility(View.VISIBLE);

                            layout_utility_mobileno.setHint("Service Number");
                            layout_utility_mobilenoo.setHint("City");

                        } else if (i == 5) {
                            layout_utility_mobilenoo.setVisibility(View.VISIBLE);

                            layout_utility_mobileno.setHint("Number");
                            layout_utility_mobilenoo.setHint("Account Number");

                        } else if (i == 6 || i == 7) {
                            layout_utility_mobilenoo.setVisibility(View.VISIBLE);

                            layout_utility_mobileno.setHint("Policy Number");
                            layout_utility_mobilenoo.setHint("Date of Birth");

                        } else if (i == 8 || i == 9 || i == 10 || i == 13 || i == 14 || i == 15 || i == 16 || i == 17 || i == 20 || i == 22 || i == 25 || i == 26) {
                            layout_utility_mobileno.setHint("Customer number");
                        } else if (i == 11 || i == 23) {
                            layout_utility_mobileno.setHint("Service Number");
                        } else if (i == 12) {
                            layout_utility_mobileno.setHint("K Number");
                        } else if (i == 18) {
                            layout_utility_mobileno.setHint("Consumer ID");
                        } else if (i == 19 || i == 21) {
                            layout_utility_mobileno.setHint("Business Partener Number");
                        } else if (i == 24) {
                            layout_utility_mobileno.setHint("Customer ID");
                        }

                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.utility_fetch_bill_button:
                rechargeUtilityFetchBill();
                break;
            case R.id.utility_button:
                rechargeUtilitySubmit();
                break;
            default:
                break;
        }

    }


    private void rechargeUtilityFetchBill() {
        showLoader();
        final UtilityRequestFetchBill utilityRequestFetchBill = new UtilityRequestFetchBill(stdCodeString, pincodeString, amountString, mobilenoString, accountNoString, latitudeString, circleCodeString, operatorString, rechargetypeString, authString, longitudeString);

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_FETCH_BILL_AMOUNT_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            //  performEncodedBillAmount(utilityRequestFetchBill,encodedUrl);
                            proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    public void proceedRecharge(String url) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode", stdCodeString);
            obj.put("amount", amountString);
            obj.put("mobileNumber", mobilenoString);
            obj.put("accountNo", accountNoString);
            obj.put("circleCode", circleCodeString);
            obj.put("operatorCode", operatorString);
            obj.put("rechargeType", rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", _token)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if (obj.getString("status").equalsIgnoreCase("0")) {

                                    utility_button.setVisibility(View.VISIBLE);
                                    utility_fetch_bill_button.setVisibility(View.GONE);
                                    amountString = obj.getString("statusDesc");
                                    utility_amount.setText("Amount: " + amountString);
                                    SuccessDialog(amountString);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message = gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            ErrorDialog("RECHARGE FAILED! " + message.getMessage());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void rechargeUtilitySubmit() {
        showLoader();
        final UtilityRequestSubmit utilityRequestSubmit = new UtilityRequestSubmit(stdCodeString, pincodeString, amountString, mobilenoString, accountNoString, latitudeString, circleCodeString, operatorString, rechargetypeString, authString, longitudeString);
        if (this.apiService == null) {
            this.apiService = new APIService();
        }

        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            performEncodedRecharge(utilityRequestSubmit, encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void performEncodedRecharge(UtilityRequestSubmit utilityRequestSubmit, String encodedUrl) {

        final UtilityApiSubmit utilityApiSubmit = this.apiService.getClient().create(UtilityApiSubmit.class);

        utilityApiSubmit.getUtilitySubmit(_token, utilityRequestSubmit, encodedUrl).enqueue(new Callback<UtilityResponseSubmit>() {
            @Override
            public void onResponse(Call<UtilityResponseSubmit> call, Response<UtilityResponseSubmit> response) {
                if (response.isSuccessful()) {
                    Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566" + response.body());
                    utility_button.setVisibility(View.GONE);
                    utility_fetch_bill_button.setVisibility(View.VISIBLE);
                    hideLoader();
                    SuccessDialog(response.body().getStatusDesc());
                } else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedVerifyOtp1234577" + response.body());
                    Gson gson = new Gson();
                    MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    ErrorDialog("RECHARGE FAILED! " + message.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UtilityResponseSubmit> call, Throwable t) {
                hideLoader();
                Log.d("STATUS", "TransactionFailedVerifyOtp1234588" + t);
                ErrorDialog("RECHARGE FAILED! undefined");

            }
        });

    }

    private void ErrorDialog(String msg) {

        clear();

        Intent intent = new Intent(UtilityActivity.this, ReacargeStatusActivity.class);
        intent.putExtra("title", "Prepaid Mobile");
        intent.putExtra("status", "false");
        intent.putExtra("message", msg);
        startActivity(intent);
        /*AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        clear();
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }

    private void SuccessDialog(String msg) {

        clear();

        Intent intent = new Intent(UtilityActivity.this, ReacargeStatusActivity.class);
        intent.putExtra("title", "Prepaid Mobile");
        intent.putExtra("status", "true");
        intent.putExtra("message", msg);
        startActivity(intent);

     /*   AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                clear();
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }

    public void showLoader() {
        pd = new ProgressDialog(UtilityActivity.this);
        pd.setMessage("loading");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void hideLoader() {
        pd.hide();
    }

    private void clear() {
        rechargetypeString = "Utility_bills";
        stdCodeString = "";
        pincodeString = "";
        accountNoString = "";
        latitudeString = "";
        circleCodeString = "";
        longitudeString = "";
        authString = "";
    }
}
