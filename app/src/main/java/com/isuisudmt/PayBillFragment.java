package com.isuisudmt;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.isuisudmt.electricity.Electricity_bill_Activity;
import com.isuisudmt.utility.UtilityActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PayBillFragment extends Fragment {

    View view;
    LinearLayout elctricity,metro,gas;

    public PayBillFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_pay_bill, container, false);
        elctricity = view.findViewById(R.id.utility);
        metro = view.findViewById(R.id.metro);
        gas = view.findViewById(R.id.gas);

        elctricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), UtilityActivity.class));
//                startActivity(new Intent(getActivity(), Electricity_bill_Activity.class));
                Toast.makeText(getContext(), "Coming Soon !", Toast.LENGTH_SHORT).show();
            }
        });

        metro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(getActivity(), UtilityActivity.class));
            }
        });

        gas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getActivity(), DataCardActivity.class));
            }
        });

        return view;
    }
}