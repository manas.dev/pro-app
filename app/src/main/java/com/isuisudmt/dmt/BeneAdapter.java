package com.isuisudmt.dmt;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.isuisudmt.R;

import java.util.ArrayList;
import java.util.HashMap;

public class BeneAdapter extends ArrayAdapter {

    Context context;
    int resource, textViewResourceId;
    ArrayList<HashMap<String, String>> items, tempItems, suggestions;

    public BeneAdapter(Context context, int resource, int textViewResourceId, ArrayList<HashMap<String, String>> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<HashMap<String, String>>(items);
        suggestions = new ArrayList<HashMap<String, String>>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.rowlayout, parent, false);
        }

        HashMap<String, String> people = items.get(position);
        TextView name = view.findViewById(R.id.name);
        TextView beneMobileNo = view.findViewById(R.id.beneMobileNo);
        TextView accountNo = view.findViewById(R.id.accountNo);
        TextView bankName = view.findViewById(R.id.bankName);

        name.setText("Name: " + people.get("beneName"));
        beneMobileNo.setText("Mobile:" + people.get("beneMobileNo"));
        accountNo.setText("Acc. No.: " + people.get("accountNo"));
        bankName.setText("Bank: " + people.get("bankName"));

        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Log.i("resultValue ::", "" + resultValue);
            String str = ((HashMap<String, String>) resultValue).get("beneMobileNo");
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (HashMap<String, String> people : tempItems) {
                    if (people.get("beneMobileNo").toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<HashMap<String, String>> filterList = (ArrayList<HashMap<String, String>>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (HashMap<String, String> people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
}