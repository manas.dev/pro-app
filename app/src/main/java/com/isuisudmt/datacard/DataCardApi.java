package com.isuisudmt.datacard;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface DataCardApi {
    @POST()
    Call<DataCardResponse> getDataCard(@Header("Authorization") String token, @Body DataCardRequest dataCardRequest, @Url String url);
}
