package com.isuisudmt.insurance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.paxsz.easylink.model.AppSelectResponse;

import java.util.ArrayList;
import java.util.Iterator;

public class InsuranaceReportAdapter extends RecyclerView.Adapter<InsuranaceReportAdapter.ViewHolder> implements Filterable {
    /* access modifiers changed from: private */
    public Context context;
    InsurancePDFListener pdf_listener;
    ArrayList<InsuranceReportModel> reportModels;
    ArrayList<InsuranceReportModel> reportModelsFilter;

    public interface InsurancePDFListener {
        void onCOI_Click(String str);

        void onDOGH_Click(String str);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView accNo;
        TextView amount_balance_txt;
        TextView amount_transact_txt;
        TextView api_comment;
        TextView applicationID;
        TextView bankName;
        TextView coi;
        TextView dateTime;
        TextView dogh;
        TextView operation_perf;
        LinearLayout pdf_ll;
        TextView previous_amount;
        TextView route;
        TextView statusTextView;
        TextView tnxType;
        TextView transType_txt;
        TextView username;

        public ViewHolder(View itemView) {
            super(itemView);
            this.dateTime = (TextView) itemView.findViewById(R.id.dateTime);
            this.statusTextView = (TextView) itemView.findViewById(R.id.statusTextView);
            this.bankName = (TextView) itemView.findViewById(R.id.bankName);
            this.amount_transact_txt = (TextView) itemView.findViewById(R.id.amount_transact_txt);
            this.amount_balance_txt = (TextView) itemView.findViewById(R.id.amount_balance_txt);
            this.previous_amount = (TextView) itemView.findViewById(R.id.amount);
            this.accNo = (TextView) itemView.findViewById(R.id.accNo);
            this.tnxType = (TextView) itemView.findViewById(R.id.tnxType);
            this.route = (TextView) itemView.findViewById(R.id.route);
            this.api_comment = (TextView) itemView.findViewById(R.id.api_comment);
            this.operation_perf = (TextView) itemView.findViewById(R.id.operation_perf);
            this.transType_txt = (TextView) itemView.findViewById(R.id.transType_txt);
            this.username = (TextView) itemView.findViewById(R.id.username);
            this.pdf_ll = (LinearLayout) itemView.findViewById(R.id.pdf_ll);
            this.dogh = (TextView) itemView.findViewById(R.id.dogh);
            this.coi = (TextView) itemView.findViewById(R.id.coi);
            this.applicationID = (TextView) itemView.findViewById(R.id.applicationID);
        }
    }

    public InsuranaceReportAdapter(ArrayList<InsuranceReportModel> reportModels2, Context context2, InsurancePDFListener listener) {
        this.reportModels = reportModels2;
        this.reportModelsFilter = reportModels2;
        this.context = context2;
        this.pdf_listener = listener;
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    InsuranaceReportAdapter insuranaceReportAdapter = InsuranaceReportAdapter.this;
                    insuranaceReportAdapter.reportModelsFilter = insuranaceReportAdapter.reportModels;
                } else {
                    ArrayList<InsuranceReportModel> filteredList = new ArrayList<>();
                    Iterator it = InsuranaceReportAdapter.this.reportModels.iterator();
                    while (it.hasNext()) {
                        InsuranceReportModel row = (InsuranceReportModel) it.next();
                        if (row.getOperationPerformed().toLowerCase().contains(charString.toLowerCase()) || row.getOperationPerformed().contains(charSequence)) {
                            filteredList.add(row);
                        }
                        if (String.valueOf(row.getId()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getId()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    InsuranaceReportAdapter.this.reportModelsFilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = InsuranaceReportAdapter.this.reportModelsFilter;
                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                InsuranaceReportAdapter.this.reportModelsFilter = (ArrayList) filterResults.values;
                InsuranaceReportAdapter.this.notifyDataSetChanged();
            }
        };
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.insurance_report_row, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        String str = "₹";
        try {
            holder.itemView.setTag(this.reportModelsFilter.get(position));
            final InsuranceReportModel pu = (InsuranceReportModel) this.reportModelsFilter.get(position);
            TextView textView = holder.tnxType;
            StringBuilder sb = new StringBuilder();
            sb.append("Txn ID: ");
            sb.append(pu.getId());
            textView.setText(sb.toString());
            TextView textView2 = holder.operation_perf;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Operation : ");
            sb2.append(pu.getOperationPerformed());
            textView2.setText(sb2.toString());
            TextView textView3 = holder.previous_amount;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(pu.getPreviousAmount());
            textView3.setText(String.valueOf(sb3.toString()));
            TextView textView4 = holder.amount_transact_txt;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append(pu.getAmountTransacted());
            textView4.setText(String.valueOf(sb4.toString()));
            TextView textView5 = holder.amount_balance_txt;
            StringBuilder sb5 = new StringBuilder();
            sb5.append(str);
            sb5.append(pu.getBalanceAmount());
            textView5.setText(String.valueOf(sb5.toString()));
            TextView textView6 = holder.dateTime;
            StringBuilder sb6 = new StringBuilder();
            sb6.append("Date: ");
            sb6.append(Util.getDateFromTime(pu.getCreatedDate()));
            textView6.setText(sb6.toString());
            holder.statusTextView.setText(pu.getStatus());
            TextView textView7 = holder.username;
            StringBuilder sb7 = new StringBuilder();
            sb7.append("Username: ");
            sb7.append(pu.getUserName());
            textView7.setText(sb7.toString());
            holder.transType_txt.setText(pu.getTransactionType());
            if (pu.getStatus().equalsIgnoreCase(AppSelectResponse.SUCCESS)) {
                holder.statusTextView.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
            } else {
                holder.statusTextView.setTextColor(ContextCompat.getColor(this.context, R.color.red));
            }
            if (pu.getOperationPerformed().equalsIgnoreCase("INSURANCE_SAMPOORNABIMA")) {
                holder.pdf_ll.setVisibility(View.GONE);
                holder.applicationID.setVisibility(View.VISIBLE);
            } else {
                holder.pdf_ll.setVisibility(View.VISIBLE);
                holder.applicationID.setVisibility(View.VISIBLE);
            }
            holder.dogh.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (pu.getApplicationId().equalsIgnoreCase("null")) {
                        Toast.makeText(context, "Application id is null !", Toast.LENGTH_LONG).show();
                    } else {
                        pdf_listener.onDOGH_Click(pu.getApplicationId());
                    }
                }
            });
            holder.coi.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (pu.getApplicationId().equalsIgnoreCase("null")) {
                        Toast.makeText(context, "Application id is null !", Toast.LENGTH_LONG).show();
                    } else {
                        pdf_listener.onCOI_Click(pu.getApplicationId());
                    }
                }
            });
            if (pu.getApplicationId().equalsIgnoreCase("null")) {
                holder.applicationID.setText("Application ID : null");
                return;
            }
            TextView textView8 = holder.applicationID;
            StringBuilder sb8 = new StringBuilder();
            sb8.append("Application ID : ");
            sb8.append(pu.getApplicationId());
            textView8.setText(sb8.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getItemCount() {
        return this.reportModelsFilter.size();
    }
}
