package com.isuisudmt.insurance;

import android.util.Base64;

import androidx.core.app.NotificationCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.load.Key;
import com.google.common.net.HttpHeaders;
import com.isuisudmt.insurance.InsuranceReportContract.View;
import com.isuisudmt.matm.MicroReportModel;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

public class InsuranceReportPresenter implements InsuranceReportContract.UserActionsListener {
    private AEPSAPIService aepsapiService;
    View insuranceReportContractView;
    private ArrayList<MicroReportModel> microReportModelArrayList;

    public InsuranceReportPresenter(View insuranceReportContractView2) {
        this.insuranceReportContractView = insuranceReportContractView2;
    }

    public void loadReports(String fromDate, String toDate, String token, String transactionType, String matmtype, ArrayList<InsuranceReportModel> insuranceReportModelArrayList) {
        String str = toDate;
        if (fromDate != null) {
            String str2 = "";
            if (!fromDate.matches(str2) && str != null && !toDate.matches(str2)) {
                this.insuranceReportContractView.showLoader();
                if (this.aepsapiService == null) {
                    this.aepsapiService = new AEPSAPIService();
                }
                ANRequest build = AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67").setPriority(Priority.HIGH).build();
                final String str3 = fromDate;
                final String str4 = toDate;
                final String str5 = token;
                final String str6 = transactionType;
                final String str7 = matmtype;
                final ArrayList<InsuranceReportModel> arrayList = insuranceReportModelArrayList;
                JSONObjectRequestListener r0 = new JSONObjectRequestListener() {
                    public void onResponse(JSONObject response) {
                        try {
                            String key = new JSONObject(response.toString()).getString("hello");
                            PrintStream printStream = System.out;
                            StringBuilder sb = new StringBuilder();
                            sb.append(">>>>-----");
                            sb.append(key);
                            printStream.println(sb.toString());
                            InsuranceReportPresenter.this.encryptedReport(str3, str4, str5, str6, new String(Base64.decode(key, 0), Key.STRING_CHARSET_NAME), str7, arrayList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e2) {
                            e2.printStackTrace();
                        }
                    }

                    public void onError(ANError anError) {
                    }
                };
                build.getAsJSONObject(r0);
                return;
            }
        }
        this.insuranceReportContractView.emptyDates();
    }

    public void refreshReports(String token, String amount, String transactionType, String transactionMode, String clientUniqueId) {
    }

    public void encryptedReport(String fromDate, String toDate, String token, String type, String encodedUrl, final String matmtype, ArrayList<InsuranceReportModel> arrayList) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType", type);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);
            AndroidNetworking.post(encodedUrl).setPriority(Priority.HIGH).addHeaders(HttpHeaders.AUTHORIZATION, token).addJSONObjectBody(jsonObject).build().getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    String str;
                    JSONArray jsonArray;
                    JSONObject obj;
                    String str2 = "transactionType";
                    String str3 = "operationPerformed";
                    String str4 = "null";
                    try {
                        JSONObject obj2 = new JSONObject(response.toString());
                        JSONArray jsonArray2 = obj2.getJSONArray("BQReport");
                        InsuranceReportResponse reportResponse = new InsuranceReportResponse();
                        ArrayList<InsuranceReportModel> reportModels = new ArrayList<>();
                        int i = 0;
                        while (i < jsonArray2.length()) {
                            InsuranceReportModel reportModel = new InsuranceReportModel();
                            JSONObject jsonObject1 = jsonArray2.getJSONObject(i);
                            if (matmtype.equalsIgnoreCase(jsonObject1.getString(str3))) {
                                reportModel.setId(jsonObject1.getString("Id"));
                                String previousAmount = jsonObject1.getString("previousAmount");
                                if (!previousAmount.equalsIgnoreCase(str4)) {
                                    reportModel.setPreviousAmount(Double.valueOf(previousAmount));
                                } else {
                                    reportModel.setPreviousAmount(str4);
                                }
                                String balanceAmount = jsonObject1.getString("balanceAmount");
                                if (!balanceAmount.equalsIgnoreCase(str4)) {
                                    reportModel.setBalanceAmount(Double.valueOf(balanceAmount));
                                } else {
                                    reportModel.setBalanceAmount(str4);
                                }
                                StringBuilder sb = new StringBuilder();
                                sb.append("");
                                sb.append(jsonObject1.getString("amountTransacted"));
                                String amountTransacted = sb.toString();
                                if (!amountTransacted.equalsIgnoreCase(str4)) {
                                    reportModel.setAmountTransacted(Integer.valueOf(Integer.parseInt(amountTransacted)));
                                } else {
                                    reportModel.setAmountTransacted(Integer.valueOf(Integer.parseInt("0")));
                                }
                                String apiComment = jsonObject1.getString("apiTId");
                                if (!apiComment.equalsIgnoreCase(str4)) {
                                    reportModel.setApiComment(apiComment);
                                } else {
                                    reportModel.setApiComment(str4);
                                }
                                obj = obj2;
                                reportModel.setStatus(jsonObject1.getString(NotificationCompat.CATEGORY_STATUS));
                                String trasaction_type = jsonObject1.getString(str2);
                                jsonArray = jsonArray2;
                                reportModel.setTransactionType(jsonObject1.optString(str2));
                                if (!trasaction_type.equalsIgnoreCase(str4)) {
                                    reportModel.setTransactionType(trasaction_type);
                                } else {
                                    reportModel.setTransactionType(str4);
                                }
                                String operationPerformed = jsonObject1.getString(str3);
                                if (!operationPerformed.equalsIgnoreCase(str4)) {
                                    reportModel.setOperationPerformed(operationPerformed);
                                } else {
                                    reportModel.setOperationPerformed(str4);
                                }
                                str = str2;
                                reportModel.setMasterName(jsonObject1.getString("masterName"));
                                reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                                reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                                reportModels.add(reportModel);
                            } else {
                                str = str2;
                                obj = obj2;
                                jsonArray = jsonArray2;
                            }
                            i++;
                            obj2 = obj;
                            jsonArray2 = jsonArray;
                            str2 = str;
                        }
                        JSONArray jSONArray = jsonArray2;
                        Collections.reverse(reportModels);
                        reportResponse.setmATMTransactionReport(reportModels);
                        if (reportResponse.getmATMTransactionReport() != null) {
                            ArrayList<InsuranceReportModel> result = reportResponse.getmATMTransactionReport();
                            double totalAmount = 0.0d;
                            for (int i2 = 0; i2 < result.size(); i2++) {
                                totalAmount += Double.parseDouble(String.valueOf(((InsuranceReportModel) result.get(i2)).getAmountTransacted()));
                            }
                            InsuranceReportPresenter.this.insuranceReportContractView.reportsReady(result, String.valueOf(totalAmount));
                        }
                        InsuranceReportPresenter.this.insuranceReportContractView.hideLoader();
                        InsuranceReportPresenter.this.insuranceReportContractView.showReports();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        InsuranceReportPresenter.this.insuranceReportContractView.hideLoader();
                        InsuranceReportPresenter.this.insuranceReportContractView.showReports();
                    }
                }

                public void onError(ANError anError) {
                    anError.getErrorDetail();
                    InsuranceReportPresenter.this.insuranceReportContractView.hideLoader();
                    InsuranceReportPresenter.this.insuranceReportContractView.showReports();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
