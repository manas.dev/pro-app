package com.isuisudmt.insurance;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InsuranceReportResponse {
    @SerializedName("BQReport")
    private ArrayList<InsuranceReportModel> mATMTransactionReport;

    public ArrayList<InsuranceReportModel> getmATMTransactionReport() {
        return this.mATMTransactionReport;
    }

    public void setmATMTransactionReport(ArrayList<InsuranceReportModel> mATMTransactionReport2) {
        this.mATMTransactionReport = mATMTransactionReport2;
    }
}
