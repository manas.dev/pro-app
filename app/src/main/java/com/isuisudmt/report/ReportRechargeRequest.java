package com.isuisudmt.report;

public class ReportRechargeRequest {
    private String fromDate;
    private String toDate;
    private String transactionType;

    public ReportRechargeRequest(String transactionType2, String fromDate2, String toDate2) {
        this.transactionType = transactionType2;
        this.fromDate = fromDate2;
        this.toDate = toDate2;
    }

    public String getTransactionType() {
        return this.transactionType;
    }

    public void setTransactionType(String transactionType2) {
        this.transactionType = transactionType2;
    }

    public String getFromDate() {
        return this.fromDate;
    }

    public void setFromDate(String fromDate2) {
        this.fromDate = fromDate2;
    }

    public String getToDate() {
        return this.toDate;
    }

    public void setToDate(String toDate2) {
        this.toDate = toDate2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ClassPojo [transactionType = ");
        sb.append(this.transactionType);
        sb.append(", fromDate = ");
        sb.append(this.fromDate);
        sb.append(", toDate = ");
        sb.append(this.toDate);
        sb.append("]");
        return sb.toString();
    }
}
