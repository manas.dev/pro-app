package com.isuisudmt.report;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.paxsz.easylink.model.AppSelectResponse;

import java.util.ArrayList;
import java.util.List;

public class CustomReportAdapterRecharge extends RecyclerView.Adapter<CustomReportAdapterRecharge.ViewHolder> implements Filterable {
    private Context context;
    /* access modifiers changed from: private */
    public List<RechargeResponseSetGet> rechargeResponseSetGets;
    /* access modifiers changed from: private */
    public List<RechargeResponseSetGet> rechargeResponseSetGetsfilter;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView api_comment;
        public TextView mobile_number_recharge;
        public TextView operator_description_recharge;
        public TextView status;
        public TextView transaction_id_recharge;
        public TextView transaction_type_recharge;
        public TextView updated_date;

        public ViewHolder(View itemView) {
            super(itemView);
            transaction_id_recharge =  itemView.findViewById(R.id.transaction_id_recharge);
            operator_description_recharge =  itemView.findViewById(R.id.operator_description_recharge);
            mobile_number_recharge =  itemView.findViewById(R.id.mobile_number_recharge);
            amount_transacted_recharge =  itemView.findViewById(R.id.amount_transacted_recharge);
            transaction_type_recharge =  itemView.findViewById(R.id.transaction_type_recharge);
            status =  itemView.findViewById(R.id.status);
            updated_date =  itemView.findViewById(R.id.updated_date);
            api_comment =  itemView.findViewById(R.id.api_comment);
        }
    }

    public CustomReportAdapterRecharge(Context context2, List rechargeResponseSetGets2) {
        this.context = context2;
        this.rechargeResponseSetGets = rechargeResponseSetGets2;
        this.rechargeResponseSetGetsfilter = rechargeResponseSetGets2;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.report_row_recharge, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RechargeResponseSetGet pu = rechargeResponseSetGetsfilter.get(position);

        TextView textView = holder.transaction_id_recharge;
        StringBuilder sb = new StringBuilder();
        sb.append("Transaction Id: ");
        sb.append(String.valueOf(pu.getId()));
        textView.setText(sb.toString());
        TextView textView2 = holder.operator_description_recharge;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Operator: ");
        sb2.append(pu.getOperatorDescription());
        textView2.setText(sb2.toString());
        TextView textView3 = holder.mobile_number_recharge;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Mobile No:");
        sb3.append(pu.getMobileNumber());
        textView3.setText(sb3.toString());
        TextView textView4 = holder.amount_transacted_recharge;
        StringBuilder sb4 = new StringBuilder();
        sb4.append("₹ ");
        sb4.append(pu.getAmountTransacted());
        textView4.setText(String.valueOf(sb4.toString()));
        TextView textView5 = holder.transaction_type_recharge;
        StringBuilder sb5 = new StringBuilder();
        sb5.append("Transaction Type: ");
        sb5.append(pu.getTransactionType());
        textView5.setText(sb5.toString());
        holder.status.setText(pu.getStatus());
        TextView textView6 = holder.updated_date;
        StringBuilder sb6 = new StringBuilder();
        sb6.append("Date: ");
        sb6.append(Util.getDateFromTime(pu.getUpdatedDate()));
        textView6.setText(sb6.toString());
        if (pu.getStatus().equals(AppSelectResponse.SUCCESS)) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.red));
        }
        if (pu.getApiComment() != null) {
            TextView textView7 = holder.api_comment;
            StringBuilder sb7 = new StringBuilder();
            sb7.append("");
            sb7.append(pu.getApiComment());
            textView7.setText(sb7.toString());
        }
    }

    public int getItemCount() {
        return this.rechargeResponseSetGetsfilter.size();
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    CustomReportAdapterRecharge customReportAdapterRecharge = CustomReportAdapterRecharge.this;
                    customReportAdapterRecharge.rechargeResponseSetGetsfilter = customReportAdapterRecharge.rechargeResponseSetGets;
                } else {
                    List<RechargeResponseSetGet> filteredList = new ArrayList<>();
                    for (RechargeResponseSetGet row : CustomReportAdapterRecharge.this.rechargeResponseSetGets) {
                        if (row.getId().contains(charString.toLowerCase()) || row.getId().contains(charSequence)) {
                            filteredList.add(row);
                        }
                        if (String.valueOf(row.getMobileNumber()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getMobileNumber()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    CustomReportAdapterRecharge.this.rechargeResponseSetGetsfilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = CustomReportAdapterRecharge.this.rechargeResponseSetGetsfilter;
                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                rechargeResponseSetGetsfilter = (ArrayList) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
