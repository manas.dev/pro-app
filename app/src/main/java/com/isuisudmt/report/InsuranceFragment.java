package com.isuisudmt.report;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.SearchView.OnQueryTextListener;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener;
import com.bumptech.glide.load.Key;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.net.HttpHeaders;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.dmt.CustomReportAdapter;
import com.isuisudmt.dmt.FinoTransactionReports;
import com.isuisudmt.dmt.ReportFragmentRequest;
import com.isuisudmt.insurance.InsuranaceReportAdapter;
import com.isuisudmt.insurance.InsuranaceReportAdapter.InsurancePDFListener;
import com.isuisudmt.insurance.InsuranceReportContract.View;
import com.isuisudmt.insurance.InsuranceReportModel;
import com.isuisudmt.insurance.InsuranceReportPresenter;
import com.isuisudmt.insurance.InsuranceReportResponse;
import com.isuisudmt.matm.RefreshModel;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

public class InsuranceFragment extends Fragment implements View, OnDateSetListener, InsurancePDFListener {
    int EXTERNAL_STORAGE_CODE = 3;
    ReportDashboardActivity activity;
    private AEPSAPIService apiService;
    String coiURL = "https://itpl.iserveu.tech/generate/v94";
    Context context;
    String doghURL = "https://itpl.iserveu.tech/generate/v93";
    ArrayList<FinoTransactionReports> finoTransactionReports;
    LayoutManager layoutManager;
    InsurancePDFListener listener;
    private InsuranceReportPresenter mActionsListener;
    InsuranaceReportAdapter mAdapter;
    RelativeLayout main_rl;
    TextView noData;
    String operationType = "SAMPOORNA";
    ProgressBar progressV;
    ReportFragmentRequest reportFragmentRequest;
    /* access modifiers changed from: private */
    public RecyclerView reportRecyclerView;
    ArrayList<InsuranceReportModel> reportResponseArrayList;
    SessionManager session;
    String tokenStr = "";
    String transactionType = "INSURANCE";
    AutoCompleteTextView transaction_spinner;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(R.layout.insurance_fragment, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(android.view.View rootView) {
        ((ReportDashboardActivity) getActivity()).getSupportActionBar().setTitle("Insurance Report");
        session = new SessionManager(getActivity());
        tokenStr =  session.getUserDetails().get(SessionManager.KEY_TOKEN);
        progressV =  rootView.findViewById(R.id.progressV);
        noData =  rootView.findViewById(R.id.noData);
        transaction_spinner =  rootView.findViewById(R.id.transaction_spinner);
        main_rl =  rootView.findViewById(R.id.main_rl);
        reportRecyclerView =  rootView.findViewById(R.id.reportRecyclerView);
        reportResponseArrayList = new ArrayList<>();
        reportRecyclerView.setHasFixedSize(true);
        mActionsListener = new InsuranceReportPresenter(this);
        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);
        isReadStoragePermissionGranted();
        isWriteStoragePermissionGranted();
        transaction_spinner.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {
                try {
                    if (mAdapter != null) {
                        mAdapter.getFilter().filter(editable.toString());
                        if (mAdapter.getItemCount() == 0) {
                            noData.setVisibility(android.view.View.VISIBLE);
                        } else {
                            noData.setVisibility(android.view.View.GONE);
                        }
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    @Override
    public void onAttach(Context context2) {
        super.onAttach(context2);
        this.activity = (ReportDashboardActivity) context2;
        this.listener = this;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id != R.id.filterBar) {
            return super.onOptionsItemSelected(item);
        } else {
            callFilterFunction(item);
            return true;
        }
    }

    private void callFilterFunction(MenuItem item) {
        final MenuItem myActionMenuItem = item;
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                FragmentActivity activity = InsuranceFragment.this.getActivity();
                StringBuilder sb = new StringBuilder();
                sb.append("SearchOnQueryTextSubmit: ");
                sb.append(query);
                Toast.makeText(activity, sb.toString(), Toast.LENGTH_LONG).show();
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            public boolean onQueryTextChange(String s) {
                if (reportResponseArrayList == null || reportResponseArrayList.size() <= 0) {
                    Toast.makeText(getActivity(), InsuranceFragment.this.getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                } else {
                    mAdapter.getFilter().filter(s);
                    mAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);
    }

    private void callCalenderFunction() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(this, now.get(1), now.get(2), now.get(5));
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        dpd.setMaxDate(Calendar.getInstance());
        dpd.setAutoHighlight(true);
    }

    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        int i = year;
        int i2 = dayOfMonth;
        int i3 = yearEnd;
        int i4 = dayOfMonthEnd;
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        String str = "-";
        sb.append(str);
        sb.append(monthOfYear + 1);
        sb.append(str);
        sb.append(i2);
        String fromdate = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(i3);
        sb2.append(str);
        sb2.append(monthOfYearEnd + 1);
        sb2.append(str);
        sb2.append(i4);
        String todate = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(i3);
        sb3.append(str);
        sb3.append(monthOfYearEnd + 1);
        sb3.append(str);
        sb3.append(i4 + 1);
        String api_todate = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append(i2);
        String str2 = "/";
        sb4.append(str2);
        sb4.append(monthOfYear + 1);
        sb4.append(str2);
        sb4.append(i);
        sb4.append(" To ");
        sb4.append(i4);
        sb4.append(str2);
        sb4.append(monthOfYearEnd + 1);
        sb4.append(str2);
        sb4.append(i3);
        ((ReportDashboardActivity) getActivity()).getSupportActionBar().setSubtitle((CharSequence) sb4.toString());
        Date from_date = null;
        Date to_date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            from_date = formatter.parse(fromdate);
            to_date = formatter.parse(todate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (from_date.before(to_date) || from_date.equals(to_date)) {
            reportCall(fromdate, api_todate, this.tokenStr);
        } else {
            Toast.makeText(getActivity(), "From date should be less than To date.", Toast.LENGTH_LONG).show();
        }
    }

    private void reportCall(final String fromdate, final String todate, final String token) {
        if (apiService == null) {
            apiService = new AEPSAPIService();
        }
        if (Util.getDateDiff(new SimpleDateFormat("yyyy-MM-dd"), fromdate, todate) <= 10) {
            showLoader();
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67").setPriority(Priority.HIGH).build().getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    try {
                        String key = new JSONObject(response.toString()).getString("hello");
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append(">>>>-----");
                        sb.append(key);
                        printStream.println(sb.toString());
                        encryptedReport(fromdate, todate, token, new String(Base64.decode(key, 0), Key.STRING_CHARSET_NAME), InsuranceFragment.this.operationType);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideLoader();
                    } catch (UnsupportedEncodingException e2) {
                        e2.printStackTrace();
                    }
                }

                public void onError(ANError anError) {
                    hideLoader();
                }
            });
            return;
        }
        hideLoader();
        noData.setVisibility(android.view.View.GONE);
        if (mAdapter != null) {
            reportResponseArrayList.clear();
            mAdapter.notifyDataSetChanged();
        }
        Toast.makeText(getActivity(), "Please choose the date within 10 days from the current date ", Toast.LENGTH_LONG).show();
    }

    public void encryptedReport(String fromDate, String toDate, String token, String encodedUrl, String oprationType) {
        showLoader();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType", this.transactionType);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);
            AndroidNetworking.post(encodedUrl).setPriority(Priority.HIGH).addHeaders(HttpHeaders.AUTHORIZATION, token).addJSONObjectBody(jsonObject).build().getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    String str = "transactionType";
                    String str2 = "operationPerformed";
                    String str3 = "";
                    String str4 = "null";
                    InsuranceFragment.this.hideLoader();
                    try {
                        JSONObject obj = new JSONObject(response.toString());
                        JSONArray jsonArray = obj.getJSONArray("BQReport");
                        InsuranceReportResponse reportResponse = new InsuranceReportResponse();
                        reportResponseArrayList.clear();
                        int i = 0;
                        while (i < jsonArray.length()) {
                            InsuranceReportModel reportModel = new InsuranceReportModel();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String string = jsonObject1.getString(str2);
                            reportModel.setId(jsonObject1.getString("Id"));
                            String previousAmount = jsonObject1.getString("previousAmount");
                            String str5 = "0";
                            if (!previousAmount.equalsIgnoreCase(str4)) {
                                reportModel.setPreviousAmount(Double.valueOf(previousAmount));
                            } else {
                                reportModel.setPreviousAmount(str5);
                            }
                            String balanceAmount = jsonObject1.getString("balanceAmount");
                            if (!balanceAmount.equalsIgnoreCase(str4)) {
                                reportModel.setBalanceAmount(Double.valueOf(balanceAmount));
                            } else {
                                reportModel.setBalanceAmount(str5);
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.append(str3);
                            JSONObject obj2 = obj;
                            sb.append(jsonObject1.getString("amountTransacted"));
                            String amountTransacted = sb.toString();
                            if (!amountTransacted.equalsIgnoreCase(str4)) {
                                reportModel.setAmountTransacted(Integer.valueOf(Integer.parseInt(amountTransacted)));
                            } else {
                                reportModel.setAmountTransacted(Integer.valueOf(Integer.parseInt(str5)));
                            }
                            String apiComment = jsonObject1.getString("apiComment");
                            if (!apiComment.equalsIgnoreCase(str4)) {
                                reportModel.setApiComment(apiComment);
                            } else {
                                reportModel.setApiComment(str3);
                            }
                            String applicationId = jsonObject1.getString("applicationId");
                            if (!apiComment.equalsIgnoreCase(str4)) {
                                reportModel.setApplicationId(applicationId);
                            } else {
                                reportModel.setApplicationId(str3);
                            }
                            String str6 = amountTransacted;
                            String userName = jsonObject1.getString(SessionManager.userName);
                            if (!apiComment.equalsIgnoreCase(str4)) {
                                reportModel.setUserName(userName);
                            } else {
                                reportModel.setUserName(str3);
                            }
                            String str7 = userName;
                            reportModel.setStatus(jsonObject1.getString(NotificationCompat.CATEGORY_STATUS));
                            String trasaction_type = jsonObject1.getString(str);
                            JSONArray jsonArray2 = jsonArray;
                            reportModel.setTransactionType(jsonObject1.optString(str));
                            if (!trasaction_type.equalsIgnoreCase(str4)) {
                                reportModel.setTransactionType(trasaction_type);
                            } else {
                                reportModel.setTransactionType(str3);
                            }
                            String operationPerformed = jsonObject1.getString(str2);
                            if (!operationPerformed.equalsIgnoreCase(str4)) {
                                reportModel.setOperationPerformed(operationPerformed);
                            } else {
                                reportModel.setOperationPerformed(str3);
                            }
                            String str8 = str;
                            reportModel.setMasterName(jsonObject1.getString("masterName"));
                            reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                            reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                            reportResponseArrayList.add(reportModel);
                            i++;
                            obj = obj2;
                            jsonArray = jsonArray2;
                            str = str8;
                        }
                        JSONArray jSONArray = jsonArray;
                        Collections.reverse(InsuranceFragment.this.reportResponseArrayList);
                        reportResponse.setmATMTransactionReport(InsuranceFragment.this.reportResponseArrayList);
                        if (reportResponse.getmATMTransactionReport() != null) {
                            ArrayList<InsuranceReportModel> result = reportResponse.getmATMTransactionReport();
                            double totalAmount = 0.0d;
                            for (int i2 = 0; i2 < result.size(); i2++) {
                                totalAmount += Double.parseDouble(String.valueOf(((InsuranceReportModel) result.get(i2)).getAmountTransacted()));
                            }
                        }
                        if (reportResponseArrayList.isEmpty()) {
                            noData.setVisibility(android.view.View.VISIBLE);
                        } else {
                            noData.setVisibility(android.view.View.GONE);
                        }
                        mAdapter = new InsuranaceReportAdapter(reportResponseArrayList, getContext(), listener);
                        reportRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        InsuranceFragment.this.hideLoader();
                    }
                }

                public void onError(ANError anError) {
                    anError.getErrorDetail();
                    transaction_spinner.setVisibility(android.view.View.GONE);
                    if (reportResponseArrayList.size() <= 0) {
                        noData.setVisibility(android.view.View.VISIBLE);
                    } else {
                        noData.setVisibility(android.view.View.GONE);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reportsReady(ArrayList<InsuranceReportModel> arrayList, String totalAmount) {
    }

    public void refreshDone(RefreshModel refreshModel) {
    }

    public void showReports() {
    }

    public void showLoader() {
        try {
           progressV.setVisibility(android.view.View.VISIBLE);
        } catch (Exception e) {
        }
    }

    public void hideLoader() {
        try {
            if (isVisible()) {
                progressV.setVisibility(android.view.View.GONE);
            }
        } catch (Exception e) {
        }
    }

    public void emptyDates() {
    }

    public void refreshAdapter(RecyclerView recyclerView, CustomReportAdapter adapter) {
        adapter.notifyDataSetChanged();
    }

    public boolean isReadStoragePermissionGranted() {
            if (VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED){

                    Log.v("TAG","Permission is granted");
                    return true;
                }
                else {
                    Log.v("TAG","Permission is revoked");
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_CODE);
                    return true;
                }
            }
            else { //permission is automatically granted on sdk<23 upon installation
                Log.v("TAG","Permission is granted");
                return true;
            }
    }

    public boolean isWriteStoragePermissionGranted() {
        if (VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED){

                Log.v("TAG","Permission is granted");
                return true;
            }
            else {
                Log.v("TAG","Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_CODE);
                return true;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode ==EXTERNAL_STORAGE_CODE) {
            if (grantResults.length > 0 || grantResults[0] ==  PackageManager.PERMISSION_GRANTED) {
                Log.e("permission", "Permission Granted, Now you can use local drive .");

            } else {
                Log.e("permission", "Permission Denied, You cannot use local drive .");

            }
            return;
        }
    }

    public void generatePDF_insurance(String URL, final String applicationID, final String fileNm) {
        showLoader();
        AndroidNetworking.get(URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
            public void onResponse(JSONObject response) {
                try {
                    String key = new JSONObject(response.toString()).getString("hello");
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append(">>>>-----");
                    sb.append(key);
                    printStream.println(sb.toString());
                    generatePDF_fromEncodedURL(new String(Base64.decode(key, 0), Key.STRING_CHARSET_NAME), applicationID, fileNm);
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoader();
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                    hideLoader();
                }
            }

            public void onError(ANError anError) {
            }
        });
    }

    public void generatePDF_fromEncodedURL(String encodedURL, String applicationID, final String fileName) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("ApplicationID", applicationID);
            AndroidNetworking.post(encodedURL)
                    .setPriority(Priority.HIGH)
                    .addHeaders(HttpHeaders.AUTHORIZATION, tokenStr)
                    .addJSONObjectBody(obj).build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    hideLoader();
                    try {
                        String statusDesc = response.optString("statusDesc");
                        storetoPdfandOpen(activity, response.optString("output"), fileName);
                    } catch (Exception e) {
                        hideLoader();
                    }
                }

                public void onError(ANError anError) {
                    InsuranceFragment.this.hideLoader();
                    Snackbar.make(main_rl,  "Error occured !", Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            hideLoader();
        }
    }

    public void onDOGH_Click(String applicationID) {
        isReadStoragePermissionGranted();
        isWriteStoragePermissionGranted();
        generatePDF_insurance(doghURL, applicationID, "KOTAK_DOGH");
    }

    public void onCOI_Click(String applicationID) {
        isReadStoragePermissionGranted();
        isWriteStoragePermissionGranted();
        generatePDF_insurance(coiURL, applicationID, "KOTAK_COI");
    }

    public void storetoPdfandOpen(Context context2, String base, String filetype) {
        String fname="";
        Uri uri;
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/KOTAK");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        int n = new Random().nextInt(10000);
        if (filetype.equalsIgnoreCase("KOTAK_DOGH")) {
            fname = "dogh_insurance-"+ n +" is downloaded !";

            Snackbar.make(main_rl, fname, Snackbar.LENGTH_LONG).show();
        } else {

            fname = "coi_insurance-"+ n +" is downloaded !";

            Snackbar.make(main_rl, fname, Snackbar.LENGTH_LONG).show();
        }
        File file = new File(myDir, fname);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            out.write(Base64.decode(base, 0));
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        File imgFile = new File(new File(Environment.getExternalStorageDirectory(), "KOTAK"), fname);
        Intent sendIntent = new Intent("android.intent.action.VIEW");
        if (VERSION.SDK_INT < 24) {
            uri = Uri.fromFile(file);
        } else {
            StringBuilder sb6 = new StringBuilder();
            sb6.append("file://");
            sb6.append(imgFile);
            uri = Uri.parse(sb6.toString());
        }
        sendIntent.setDataAndType(uri, "application/pdf");
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context2.startActivity(sendIntent);
    }

    public void downloadfile(String filneNm) {
    }
}
