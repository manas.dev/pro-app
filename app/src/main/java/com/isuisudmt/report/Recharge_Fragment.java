package com.isuisudmt.report;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.SearchView.OnQueryTextListener;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener;
import com.bumptech.glide.load.Key;
import com.google.common.net.HttpHeaders;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Recharge_Fragment extends Fragment implements OnDateSetListener {
    private AEPSAPIService apiService;
    Context context;
    LayoutManager layoutManager;
    CustomReportAdapterRecharge mAdapter;
    TextView noData;
    ProgressBar progressV;
    ArrayList<RechargeResponseSetGet> rechargeResponseSetGet = new ArrayList<>();
    ReportRechargeRequest reportRechargeRequestBody;
    /* access modifiers changed from: private */
    public RecyclerView reportRecyclerView;
    SessionManager session;
    String tokenStr = "";
    String transactionType = "RECHARGE";
    AutoCompleteTextView transaction_spinner;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recharge_report, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        ((ReportDashboardActivity) getActivity()).getSupportActionBar().setTitle("Recharge Report");
        session = new SessionManager(getActivity());
        tokenStr = session.getUserDetails().get(SessionManager.KEY_TOKEN);
        progressV = rootView.findViewById(R.id.progressV);
        noData =  rootView.findViewById(R.id.noData);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);
        transaction_spinner = rootView.findViewById(R.id.transaction_spinner);
        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);
        transaction_spinner.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {
                mAdapter.getFilter().filter(editable.toString());
                if (mAdapter.getItemCount() == 0) {
                    noData.setVisibility(View.VISIBLE);
                } else {
                    noData.setVisibility(View.GONE);
                }
            }
        });
    }

    public void onAttach(Context context2) {
        super.onAttach(context2);
        this.context = context2;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id != R.id.filterBar) {
            return super.onOptionsItemSelected(item);
        } else {
            callFilterFunction(item);
            return true;
        }
    }

    private void callFilterFunction(MenuItem item) {
        final MenuItem myActionMenuItem = item;
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            public boolean onQueryTextChange(String s) {
                if (rechargeResponseSetGet == null || rechargeResponseSetGet.size() <= 0) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                } else {
                    mAdapter.getFilter().filter(s);
                    mAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);
    }

    private void callCalenderFunction() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(this, now.get(1), now.get(2), now.get(5));
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        dpd.setMaxDate(Calendar.getInstance());
        dpd.setAutoHighlight(true);
    }

    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        int i = year;
        int i2 = dayOfMonth;
        int i3 = yearEnd;
        int i4 = dayOfMonthEnd;
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        String str = "-";
        sb.append(str);
        sb.append(monthOfYear + 1);
        sb.append(str);
        sb.append(i2);
        String fromdate = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(i3);
        sb2.append(str);
        sb2.append(monthOfYearEnd + 1);
        sb2.append(str);
        sb2.append(i4);
        String todate = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(i3);
        sb3.append(str);
        sb3.append(monthOfYearEnd + 1);
        sb3.append(str);
        sb3.append(i4 + 1);
        String api_todate = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append(i2);
        String str2 = "/";
        sb4.append(str2);
        sb4.append(monthOfYear + 1);
        sb4.append(str2);
        sb4.append(i);
        sb4.append(" To ");
        sb4.append(i4);
        sb4.append(str2);
        sb4.append(monthOfYearEnd + 1);
        sb4.append(str2);
        sb4.append(i3);
        ((ReportDashboardActivity) getActivity()).getSupportActionBar().setSubtitle((CharSequence) sb4.toString());
        Date from_date = null;
        Date to_date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            from_date = formatter.parse(fromdate);
            to_date = formatter.parse(todate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (from_date.before(to_date) || from_date.equals(to_date)) {
            reportCall(fromdate, api_todate, this.tokenStr);
            showLoader();
            return;
        }
        Toast.makeText(getActivity(), "From date should be less than To date.", Toast.LENGTH_LONG).show();
    }

    private void reportCall(final String fromdate, final String todate, final String token) {
        if (this.apiService == null) {
            this.apiService = new AEPSAPIService();
        }
        this.noData.setVisibility(View.VISIBLE);
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67").setPriority(Priority.HIGH).build().getAsJSONObject(new JSONObjectRequestListener() {
            public void onResponse(JSONObject response) {
                try {
                    String key = new JSONObject(response.toString()).getString("hello");
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append(">>>>-----");
                    sb.append(key);
                    printStream.println(sb.toString());
                    encryptedReport(fromdate, todate, token, new String(Base64.decode(key, 0), Key.STRING_CHARSET_NAME));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
            }

            public void onError(ANError anError) {
            }
        });
    }

    public void encryptedReport(String fromDate, String toDate, String token, String encodedUrl) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType", this.transactionType);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);
            AndroidNetworking.post(encodedUrl).setPriority(Priority.HIGH).addHeaders(HttpHeaders.AUTHORIZATION, token).addJSONObjectBody(jsonObject).build().getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    String str = "apiComment";
                    try {
                        JSONArray jsonArray = new JSONObject(response.toString()).getJSONArray("BQReport");
                        ReportRechargeResponse reportResponse = new ReportRechargeResponse();
                        ArrayList<RechargeResponseSetGet> reportModels = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            RechargeResponseSetGet reportModel = new RechargeResponseSetGet();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            reportModel.setId(jsonObject1.getString("Id"));
                            reportModel.setPreviousAmount(Double.valueOf(jsonObject1.getString("previousAmount")));
                            reportModel.setBalanceAmount(Double.valueOf(jsonObject1.getString("balanceAmount")));
                            reportModel.setAmountTransacted(Integer.valueOf(jsonObject1.getInt("amountTransacted")));
                            reportModel.setApiComment(jsonObject1.getString(str));
                            reportModel.setStatus(jsonObject1.getString(NotificationCompat.CATEGORY_STATUS));
                            reportModel.setUserName(jsonObject1.getString(SessionManager.userName));
                            reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                            reportModel.setUpdatedDate(jsonObject1.getLong("updatedDate"));
                            reportModel.setApiComment(jsonObject1.getString(str));
                            reportModel.setApiName(jsonObject1.getString("API"));
                            reportModel.setTransactionType(jsonObject1.getString("transactionType"));
                            reportModel.setMobileNumber(jsonObject1.getString("mobileNumber"));
                            reportModel.setOperatorDescription(jsonObject1.getString("operatorDescription"));
                            reportModel.setOperatorTransactionId(jsonObject1.getString("userTrackId"));
                            reportModels.add(reportModel);
                        }
                        reportResponse.setRechargeResponseSetGet(reportModels);
                        rechargeResponseSetGet = reportResponse.getRechargeResponseSetGet();
                        for (int i2 = 0; i2 < rechargeResponseSetGet.size(); i2++) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("benilist1111");
                            sb.append(rechargeResponseSetGet);
                            Log.d("BENILIST", sb.toString());
                            mAdapter = new CustomReportAdapterRecharge(getActivity(), rechargeResponseSetGet);
                            reportRecyclerView.setAdapter(mAdapter);
                            hideLoader();
                        }
                        if (jsonArray.length() == 0) {
                            Toast.makeText(getActivity(), "No Record Found", Toast.LENGTH_LONG).show();
                            hideLoader();
                            noData.setVisibility(View.VISIBLE);
                            noData.setText("No Record found.");
                            return;
                        }
                        noData.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideLoader();
                    }
                }

                public void onError(ANError anError) {
                    anError.getErrorDetail();
                    hideLoader();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showLoader() {
        this.progressV.setVisibility(View.VISIBLE);
    }

    /* access modifiers changed from: private */
    public void hideLoader() {
        this.progressV.setVisibility(View.GONE);
    }
}
